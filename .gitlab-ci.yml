# SPDX-FileCopyrightText: 2022 Helmholtz Centre for Environmental Research (UFZ)
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: MIT

---

stages:
  - lint
  - build
  - test
  - run

default:
  image: debian:bookworm
  interruptible: true

.install_dependencies:
  before_script:
    - apt-get update && apt-get -qy install python3 python3-pip pipx
    - pipx ensurepath && source ~/.bashrc

.install_cmake:
  before_script:
    - !reference [.install_dependencies, before_script]
    - pipx install cmake

.compiler_versions:
  parallel:
    matrix:
      - VERSION: ["12", "13", "14"]

license-compliance:
  stage: lint
  before_script:
    - !reference [.install_dependencies, before_script]
    - pipx install reuse
  script:
    - reuse lint
  interruptible: true

codestyle:
  stage: lint
  before_script:
    - !reference [.install_dependencies, before_script]
    - pipx install cpplint
  script:
    - cpplint --recursive src/ tests/

build:gcc:
  extends:
    - .install_cmake
    - .compiler_versions
  stage: build
  image: gcc:${VERSION}
  script:
    - cmake -S . -B build_gcc_${VERSION}
    - cmake --build build_gcc_${VERSION}
  artifacts:
    paths:
      - build_gcc_${VERSION}

test:gcc:
  extends:
    - .install_cmake
    - .compiler_versions
  stage: test
  image: gcc:${VERSION}
  script:
    - cd build_gcc_${VERSION} && ctest
  needs:
    - job: "build:gcc"
      artifacts: true

running:
  stage: run
  image: gcc:13
  script:
    - ./build_gcc_13/bin/helloWorld
  needs:
    - job: "build:gcc"
      artifacts: true

...